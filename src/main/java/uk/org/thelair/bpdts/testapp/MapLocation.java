package uk.org.thelair.bpdts.testapp;

public class MapLocation implements Location {

  private static final double EARTH_RADIUS = 6371e3;
  private final double latitude;
  private final double longitude;

  /**
   * Constructor.
   *
   * @param mapLatitude  latitude as a decimal in degrees
   * @param mapLongitude longitude as a decimal in degrees
   */
  public MapLocation(double mapLatitude, double mapLongitude) {
    latitude = mapLatitude;
    longitude = mapLongitude;
  }

  /**
   * Get latitude.
   *
   * @return latitude in decimal degrees
   */
  public double getLatitude() {
    return latitude;
  }

  /**
   * Get longitude.
   *
   * @return longitude in decimal degrees
   */
  public double getLongitude() {
    return longitude;
  }

  /**
   * Apply Haversine formula to define distance between two points on a map.
   *
   * @param other MapLocation for origin
   * @return distance in km between origin and this location
   */
  public double distanceTo(Location other) throws InvalidLocationError {
    if (other instanceof MapLocation) {
      MapLocation otherLocation = (MapLocation) other;
      double a =
          Math.pow(Math.sin(Math.toRadians(otherLocation.getLatitude() - latitude) / 2), 2) +
              Math.cos(Math.toRadians(latitude)) *
                  Math.cos(Math.toRadians(otherLocation.latitude)) *
                  Math.pow(Math.sin(Math.toRadians(otherLocation.getLongitude() - longitude) / 2), 2);
      double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      return EARTH_RADIUS * c / 1000;
    } else {
      throw new InvalidLocationError("Target is not a map location");
    }
  }
}
