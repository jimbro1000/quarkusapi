package uk.org.thelair.bpdts.testapp;

import java.util.*;

public class CityLocations implements Locations {
  Map<String, MapLocation> sites;

  public CityLocations() {
    sites = new HashMap<>();
  }

  @Override
  public Location getLocation(String locationName) {
    if (sites.containsKey(locationName)) {
      return sites.get(locationName);
    } else {
      return new NullLocation();
    }
  }

  @Override
  public void addLocation(Location site, String siteName) throws InvalidLocationError {
    if (!(site instanceof MapLocation)) {
      throw new InvalidLocationError("given site is not a map location");
    }
    if (sites.containsKey(siteName)) {
      throw new InvalidLocationError("duplicate site");
    }
    sites.put(siteName, (MapLocation) site);
  }
}
