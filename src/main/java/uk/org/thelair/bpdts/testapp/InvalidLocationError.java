package uk.org.thelair.bpdts.testapp;

public class InvalidLocationError extends Exception {
  InvalidLocationError(String errorCauseDescription) {
    super(errorCauseDescription);
  }
}
