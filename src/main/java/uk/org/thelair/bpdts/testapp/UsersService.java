package uk.org.thelair.bpdts.testapp;

import org.eclipse.microprofile.rest.client.inject.*;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.*;

@Path("")
@RegisterRestClient
public interface UsersService {

  @GET
  @Path("/users")
  @Produces(MediaType.APPLICATION_JSON)
  List<Users> getUsers();

  @GET
  @Path("/city/{city}/users")
  @Produces(MediaType.APPLICATION_JSON)
  List<Users> getCityUsers(@PathParam String city);
}
