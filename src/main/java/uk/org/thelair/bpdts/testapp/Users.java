package uk.org.thelair.bpdts.testapp;

public class Users {

  public long id;
  public String first_name;
  public String last_name;
  public String email;
  public String ip_address;
  public float latitude;
  public float longitude;
}
