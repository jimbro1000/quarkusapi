package uk.org.thelair.bpdts.testapp;

public interface Locations {
  Location getLocation(String locationName);

  void addLocation(Location site, String siteName) throws InvalidLocationError;
}
