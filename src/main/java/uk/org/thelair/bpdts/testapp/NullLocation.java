package uk.org.thelair.bpdts.testapp;

public class NullLocation implements Location {
  @Override
  public double distanceTo(Location other) throws InvalidLocationError {
    throw new InvalidLocationError("null location used in calculation");
  }
}
