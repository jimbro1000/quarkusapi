package uk.org.thelair.bpdts.testapp;

public interface Location {
  double distanceTo(Location other) throws InvalidLocationError;
}
