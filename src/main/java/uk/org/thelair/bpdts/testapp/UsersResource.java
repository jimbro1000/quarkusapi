package uk.org.thelair.bpdts.testapp;

import org.eclipse.microprofile.rest.client.inject.*;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.*;
import java.util.stream.*;

@Path("/users")
public class UsersResource {
  public CityLocations cities;
  /**
   * Assume for simplicity that "London" is the City of London (square mile)
   */
  public static final MapLocation LONDON =
      new MapLocation(51.51279, -0.09184);

  private static final double RANGE = 80;

  public UsersResource() {
    cities = new CityLocations();
    try {
      cities.addLocation(LONDON, "London");
    } catch (InvalidLocationError ex) {
      System.out.println("Failed to assemble valid city list");
    }
  }

  @Inject
  @RestClient
  UsersService usersService;

  @GET
  @Path("/city/{city}")
  @Produces(MediaType.APPLICATION_JSON)
  public Set<Users> who(@PathParam String city) {
    Location site = cities.getLocation(city);
    if (site instanceof MapLocation) {
      return inRange(usersService.getUsers(), usersService.getCityUsers(city), (MapLocation) site, RANGE);
    } else {
      return new HashSet<>();
    }
  }

  public Set<Users> inRange(List<Users> allUsers, List<Users> cityUsers, MapLocation center, double range) {
    Set<Users> result = new HashSet<>(cityUsers);
    List<Users> filteredUsers = allUsers
        .stream()
        .filter(u -> {
          try {
            return new MapLocation(u.latitude, u.longitude).distanceTo(center) <= range;
          } catch (InvalidLocationError invalidLocationError) {
            throw new RuntimeException(invalidLocationError);
          }
        })
        .collect(Collectors.toList());
    result.addAll(filteredUsers);
    return result;
  }
}
