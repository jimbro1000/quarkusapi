package uk.org.thelair.bpdts.testapp;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.hasEntry;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("rest")
@QuarkusTest
public class UsersResourceIT {

  @DisplayName("When the service is called we expect a total of nine records to be returned")
  @Test
  void serviceReturnsNineRecords() {
    given()
        .when().get("/users/city/London")
        .then()

        .statusCode(200)
        .body("$.size()", is(9)
        );
  }

  @DisplayName("Bendix Halgarth is not in the result set")
  @Test
  void serviceDoesntReturnBendixHalgarth() {
    given()
        .when().get("/users/city/London")
        .then()
        .statusCode(200)
        .body("$", not(hasItem(allOf(
            hasEntry(equalTo("firstname"), equalTo("Bendix")),
            hasEntry(equalTo("lastname"), equalTo("Halgarth"))
            )))
        );
  }
}
