package uk.org.thelair.bpdts.testapp;

import org.junit.jupiter.api.*;

public class CityLocationsTest {
  static final MapLocation LONDON =
      new MapLocation(51.51279, -0.09184);

  Locations cities;

  @BeforeEach
  void setup() throws InvalidLocationError {
    cities = new CityLocations();
    setKnownTestCities();
  }

  private void setKnownTestCities() throws InvalidLocationError {
    cities.addLocation(LONDON, "London");
  }

  @Test
  void givenAnUnknownLocationItShouldReturnANullLocation() {
    Location result = cities.getLocation("nowhere");
    Assertions.assertTrue(result instanceof NullLocation);
  }

  @Test
  void givenAKnownLocationItShouldReturnAMapLocation() {
    Location result = cities.getLocation("London");
    Assertions.assertTrue(result instanceof MapLocation);
    Assertions.assertEquals(result, LONDON);
  }

  @Test
  void givenAValidMapLocationItAcceptsANewValue() throws InvalidLocationError {
    cities.addLocation(LONDON, "NotLondon");
    Location result = cities.getLocation("NotLondon");
    Assertions.assertEquals(result, LONDON);
  }

  @Test
  void givenAValidMapLocationItRejectsADuplicateByThrowingAnError() {
    Assertions.assertThrows(InvalidLocationError.class, () -> cities.addLocation(LONDON, "London"));
  }

  @Test
  void givenAnInvalidLocationItRejectsByThrowingAnError() {
    Assertions.assertThrows(InvalidLocationError.class, () -> cities.addLocation(new NullLocation(), "NullLondon"));
  }
}
