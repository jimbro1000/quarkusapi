package uk.org.thelair.bpdts.testapp;

import org.junit.jupiter.api.*;

public class NullLocationTest {
  @Test
  void NullLocationImplementsLocation() {
    NullLocation result = new NullLocation();
    Assertions.assertTrue(result instanceof Location);
  }

  @Test
  void NullLocationThrowsExceptionOnDistanceCalculation() {
    NullLocation result = new NullLocation();
    Assertions.assertThrows(InvalidLocationError.class, () -> result.distanceTo(result));
  }
}
