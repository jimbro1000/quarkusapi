package uk.org.thelair.bpdts.testapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserResourceTest {

  private final static MapLocation NORTH_POLE = new MapLocation(90, 0);
  private final UsersResource usersResource = new UsersResource();

  @Test
  public void inRangeAcceptsTwoListsOfUsersAndAlwaysReturnsAllOfTheCityUsers() {
    List<Users> cityUsers = appendCityUsers(new ArrayList<>());
    List<Users> otherUsers = new ArrayList<>();

    Set<Users> actual = usersResource.inRange(otherUsers, cityUsers, NORTH_POLE, 0);
    Assertions.assertArrayEquals(cityUsers.toArray(), actual.toArray());
  }

  @Test
  public void inRangeAcceptsTwoListsOfUsersAndOnlyReturnsUsersInRangeOfLocation() {
    List<Users> cityUsers = new ArrayList<>();
    List<Users> otherUsers = appendInRangeUsers(new ArrayList<>());

    Set<Users> actual = usersResource.inRange(otherUsers, cityUsers, NORTH_POLE, 80);
    Assertions.assertArrayEquals(otherUsers.toArray(), actual.toArray());
  }

  @Test
  public void inRangeAcceptsTwoListsOfUsersAndDoesntReturnsUsersNotInRangeOfLocation() {
    List<Users> cityUsers = new ArrayList<>();
    List<Users> otherUsers = appendOutOfRangeUsers(new ArrayList<>());

    Set<Users> actual = usersResource.inRange(otherUsers, cityUsers, NORTH_POLE, 80);
    Assertions.assertEquals(0, actual.size());
  }

  @Test
  public void inRangeMergesTheTwoSetsOfUsers() {
    List<Users> cityUsers = appendCityUsers(new ArrayList<>());
    List<Users> otherUsers = appendInRangeUsers(appendOutOfRangeUsers(new ArrayList<>()));

    Set<Users> actual = usersResource.inRange(otherUsers, cityUsers, NORTH_POLE, 80);
    Assertions.assertEquals(2, actual.size());
  }

  List<Users> appendInRangeUsers(List<Users> existingUsers) {
    Users userA = new Users();
    userA.id = 1;
    userA.latitude = 89.99f;
    userA.longitude = 0f;
    userA.first_name = "Santa";
    userA.last_name = "Claus";
    existingUsers.add(userA);
    return existingUsers;
  }

  List<Users> appendOutOfRangeUsers(List<Users> existingUsers) {
    Users userA = new Users();
    userA.id = 2;
    userA.latitude = 34.003135f;
    userA.longitude = -117.7228641f;
    userA.first_name = "Jane";
    userA.last_name = "Doe";
    existingUsers.add(userA);
    return existingUsers;
  }

  List<Users> appendCityUsers(List<Users> existingUsers) {
    Users userA = new Users();
    userA.id = 3;
    userA.latitude = 51.47002f;
    userA.longitude = -0.454295f;
    userA.first_name = "Ben";
    userA.last_name = "Bloggs";
    existingUsers.add(userA);
    return existingUsers;
  }
}
