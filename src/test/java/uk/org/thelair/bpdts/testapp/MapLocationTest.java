package uk.org.thelair.bpdts.testapp;

import static uk.org.thelair.bpdts.testapp.UsersResource.LONDON;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MapLocationTest {

  private static final MapLocation HEATHROW = new MapLocation(51.47002, -0.454295);
  private static final MapLocation NEW_YORK_CITY = new MapLocation(40.71427, -74.00597);

  @Test
  void givenTwoIdenticalLocationsTheDistanceShouldBeZero() throws InvalidLocationError {
    MapLocation A = new MapLocation(100, 100);
    Assertions.assertEquals(0, A.distanceTo(A));
  }

  @Test
  void givenTwoDifferentLocationsTheDistanceShouldNotBeZero() throws InvalidLocationError {
    Assertions.assertNotEquals(0, LONDON.distanceTo(HEATHROW));
  }

  @Test
  void theDistanceBetweenCityOfLondonAndHeathrowShouldBe26Kilometers() throws InvalidLocationError {
    Assertions.assertEquals(26, Math.round(LONDON.distanceTo(HEATHROW)));
  }

  @Test
  void theDistanceBetweenCityOfLondonAndNewYorkCityShouldBe5572Kilometers() throws InvalidLocationError {
    Assertions.assertEquals(5572, Math.round(LONDON.distanceTo(NEW_YORK_CITY)));
  }
}
